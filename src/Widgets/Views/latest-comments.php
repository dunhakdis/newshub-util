<?php echo $this->vars->args['before_widget']; ?>
<?php
	if (!empty($this->vars->instance['title'])) {
		echo $this->vars->args['before_title'];
			echo apply_filters( 'widget_title', $this->vars->instance['title'] );
		echo $this->vars->args['after_title'];
	}
?>
<?php
$comments = get_comments(
		$args = array(
				'number' => 5,
			) 
	); 
?>

<?php if (!empty($comments)) { ?>
<ul class="no-list">
	<?php foreach($comments as $comment){ ?>
		<li class="no-list">
			<div class="row">
				
				<?php
				//comment_post_ID
					$author_url  =  $comment->user_id >= 1 ? 
									get_author_posts_url($comment->user_id) : 
									$comment->comment_author_url;
				?>

				<div class="col-md-3 col-sm-3 col-xs-3 rm-padding-right rm-padding-left">
					<a class="black" href="<?php echo esc_url($author_url); ?>" title="<?php _e('Visit Link', 'news_hub'); ?>">
						<?php echo get_avatar($comment->user_id, 96); ?>
					</a>	
				</div>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<div class="comment-author-details">
						<div class="pull-left">
							<h6 class="uppercase headlings comment-author-name">
								<a class="black" href="<?php echo esc_url($author_url); ?>" title="<?php _e('Visit Link', 'news_hub'); ?>">
									<?php echo $comment->comment_author; ?>
								</a>
							</h6>
						</div>
						<div class="pull-right">
							<span class="comment-author-time uppercase headlings">
								<a class="comment-content-linked" href="<?php echo esc_url($author_url); ?>" title="<?php _e('Visit Link', 'news_hub'); ?>">
									<?php echo human_time_diff(strtotime($comment->comment_date), current_time('timestamp')); ?>
								</a>
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<div class="comment-content">
						<p>
							<a class="comment-content-linked" href="<?php echo get_the_permalink($comment->comment_post_ID); ?>#comment-<?php echo $comment->comment_ID;?>" title="<?php _e('Read More', 'news_hub');?>">
							<?php echo nl2br( substr(strip_tags($comment->comment_content), 0, 77) );?> 
								&hellip;
							</a>
						</p>
					</div>
				</div>
			</div>
		</li>
	<?php } ?>
</ul>
<?php } ?>

<?php echo $this->vars->args['after_widget']; ?>