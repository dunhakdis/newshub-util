<?php
/**
 * Popular Videos Widgets Template
 *
 * @version  1.0
 * @package NewsHub
 * @subpackage Widgets
 * @author  dunhakdis
 */
?>
<?php $results = $this->vars->results; ?>

<?php echo $this->vars->args['before_widget']; ?>
<?php
	if (!empty($this->vars->instance['title'])) {
		echo $this->vars->args['before_title'];
			echo apply_filters( 'widget_title', $this->vars->instance['title'] );
		echo $this->vars->args['after_title'];
	}
?>
<?php if (!empty($results)) { ?>
<ul class="widget-latest-videos no-list rm-mg-left">
	<?php global $post; ?>
	<?php foreach($results as $post) { ?>
	<?php setup_postdata($post); ?>
		<li <?php echo post_class(array('entry-post','popular')); ?>>
			<?php //echo $result->post_title; ?>
			
				<div class="featured-image">

					<?php if (has_post_thumbnail()) { ?>
						<?php echo the_post_thumbnail('news-hub-latest-video'); ?>
					<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/images/news-hub-latest-video.jpg" 
						alt="<?php _e('Featured Image', 'news_hub'); ?>" />
					<?php } ?>

					<!--video details-->
					<div class="w-featured-image-entry-header">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-2">
								<a class="white" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_permalink());?>">
									<span class="white fa-stack fa-lg">
										<i class="fa fa-circle-thin fa-stack-2x"></i>
										<i class="fa fa-play fa-stack-1x"></i>
									</span>
								</a>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-10">
								<h6 class="rm-mg-bottom">
									<a class="white" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_permalink());?>">
										<?php echo esc_attr(the_title()); ?>
									</a>
								</h6>
							</div>
						</div>
					</div>
				</div>
		</li>
	<?php } ?>
</ul>

<?php } else { ?>
<p>
	<?php _e('We\'re sorry, there are no popular videos to show at this time.', 'news_hub'); ?>
</p>
<?php } ?>
<?php echo $this->vars->args['after_widget']; ?>