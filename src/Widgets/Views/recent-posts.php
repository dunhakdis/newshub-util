<?php
/**
 * RecentPosts Widgets Template
 *
 * @version  1.0
 * @package NewsHub
 * @subpackage Widgets
 * @author  dunhakdis
 */
?>
<?php echo $this->vars->args['before_widget']; ?>
<?php
	if (!empty($this->vars->instance['title'])) {
		echo $this->vars->args['before_title'];
			echo apply_filters( 'widget_title', $this->vars->instance['title'] );
		echo $this->vars->args['after_title'];
	}
?>
<?php $max_posts = !empty($this->vars->instance['max_posts']) ? $this->vars->instance['max_posts']: 0; ?>
<?php $counter = 0 ;?>
<?php $post_classes = array('entry-post'); ?>

<?php if (have_posts()) { ?>
<ul class="widget-recent-post no-list rm-mg-left">
	<?php while(have_posts()) { ?>
	<?php the_post(); ?>
	<?php $counter++; ?>
	
	<?php if ($counter === $max_posts) { ?>
		<?php $post_classes[] = 'last'; ?>
	<?php } ?>

	<li <?php echo post_class($post_classes); ?>>
		<div class="col-md-3 col-sm-12 col-xs-3 rm-padding-right rm-padding-left">
			<a class="black" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
				<?php if (function_exists('news_hub_the_thumbnail')) { ?>
					<?php news_hub_the_thumbnail('thumbnail'); ?>
				<?php } else { ?>
					<?php the_post_thumbnail('thumbnail'); ?>
				<?php } ?>
			</a>
		</div>
		<div class="col-md-9 col-sm-12 col-xs-9 rm-padding-right">
			<div>
				<div class="latest-post-widget-meta">
					<?php if (function_exists('news_hub_the_category_list')) { ?>
						<?php news_hub_the_category_list(FALSE, 1, FALSE); ?> 
					<?php } ?>
					<div class="pull-left vertical-sepator"> | </div>
					<div class="pull-left"><span class="uppercase date"><?php echo the_time('M d'); ?></span></div>
				</div>
				
				<div class="cleearfix"></div>
			</div>
			<h6>
				<a class="black" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
					<?php echo esc_attr(the_title()); ?>
				</a>
			</h6>
		</div>
		<div class="clearfix"></div>
	</li>
	<?php } ?>
</ul>
<div class="center hide">
	<h6><a title="<?php _e('Posts Archive', 'news_hub'); ?>" href="<?php echo esc_url(get_post_type_archive_link('posts')); ?>"><?php _e('See More Posts', 'news_hub'); ?></a></h6>
</div>
<?php } ?>
<?php echo $this->vars->args['after_widget']; ?>
