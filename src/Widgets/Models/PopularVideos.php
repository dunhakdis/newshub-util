<?php
/**
 * Popular Video Class
 *
 * This widget needs the WordPress Popular <https://wordpress.org/plugins/wordpress-popular-posts> 
 * plugin to work. It simply queries for the list of video
 * and return the list of the most popular video
 *
 * @version  1.0
 * @package NewsHub
 * @subpackage Widgets
 * @author dunhakdis
 */

namespace newshub\widgets;

use NewsHubConfig;

Class PopularVideos extends \WP_Widget{

	/**
	 * The name of this Widget
	 * @var string
	 */
	public $widgetName = 'NewsHub: Popular Videos';

	/**
	 * Unique identifier for our widget
	 * @var string
	 */
	public $id = 'newshubPopularVideos';

	/**
	 * Holds variables to be displayed in the template
	 * @var stdClass
	 */
	private $vars;

	/**
	 * The default number of maximum posts
	 * @var integer
	 */
	public $max_posts = 5;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		
		$this->vars = new \stdClass;
		$this->config = new NewsHubConfig();	

		// widget actual processes
		parent::__construct($this->id, $this->widgetName);

		return $this;
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		global $wpdb;

		if (isset($instance['max_posts'])) {
			$this->max_posts = (int) $instance['max_posts'];

			// handle 0 input
			if ($this->max_posts <= 0){
				$this->max_posts = 5;
			}
		}

		// outputs the content of the widget
		
		$this->vars->args = $args;
		$this->vars->instance = $instance;

		$stmt = sprintf("SELECT post.*, postid, pageviews, term.slug 
				FROM wp_popularpostsdata as popular
				INNER JOIN wp_term_relationships as relationship
				ON relationship.object_id = popular.postid
				INNER JOIN wp_terms as term 
				ON relationship.term_taxonomy_id = term_id
				INNER JOIN wp_posts as post
				ON post.ID = postid
				WHERE term.slug = 'post-format-video'
				AND post.post_status = 'publish'
				order by pageviews desc limit %d", $this->max_posts);

		$results = $wpdb->get_results($stmt, OBJECT);

		$this->vars->results = $results;
				 
			echo $this->template();

		wp_reset_postdata();
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {

		ob_start();
		// outputs the options form on admin
		if (isset($instance['title'])) {
			$this->vars->title = $instance[ 'title' ];
		} else {
			$this->vars->title = __( 'Popular Videos', 'news_hub' );
		}

		if (isset($instance['max_posts'])) {
			$this->vars->max_posts = $instance[ 'max_posts' ];
		} else {
			$this->vars->max_posts = 5;
		}
		
		require $this->config->getSourcePath() . 'Widgets/Forms/PopularVideosForm.php';

		$output = ob_get_clean();

		echo $output;
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {

		// processes widget options to be saved
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['max_posts'] = ( ! empty( $new_instance['max_posts'] ) ) ? intval( $new_instance['max_posts'] ) : '';

		return $instance;
	}

	/**
	 * Registers the widget into WordPress
	 *
	 * @uses  register_widget() registers the widget
	 * @return object self::
	 */
	public static function registerWidget(){

		register_widget(__CLASS__);

	}

	/**
	 * Loads the template for this Widget
	 * @return string the template output
	 */
	private function template()
	{
		ob_start();

		include $this->config->getSourcePath() . 'Widgets/Views/popular-videos.php';

		$output = ob_get_clean();

		return $output;
	}
}
?>