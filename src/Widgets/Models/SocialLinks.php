<?php
/**
 * This file declares the class
 * of the SocialLinks Widgets
 *
 * @version  1.0
 * @package NewsHub
 * @subpackage Widgets
 * @author dunhakdis
 */

namespace newshub\widgets;

use NewsHubConfig;

/**
 * Adds news_hub_social widget.
 */
class SocialLinks extends \WP_Widget {
	
	/**
	 * List of available social media
	 */
	var $supported_social_media = array();

	function __construct() {

		$this->supported_social_media = array(
			array( 'label' => 'Facebook', 'id' => 'facebook' ),
			array( 'label' => 'Twitter', 'id' => 'twitter' ),
			array( 'label' => 'Google Plus', 'id' => 'google-plus' ),
			array( 'label' => 'YouTube', 'id' => 'youtube' ),
			array( 'label' => 'Tumblr', 'id' => 'tumblr' ),
			array( 'label' => 'Vimeo', 'id' => 'vimeo-square' ),
			array( 'label' => 'Dribbble', 'id' => 'dribbble' ),
			array( 'label' => 'Github', 'id' => 'github' ),
			array( 'label' => 'LinkedIn', 'id' => 'linkedin' ),
			array( 'label' => 'Pinterest', 'id' => 'pinterest' ),
			array( 'label' => 'Foursquare', 'id' => 'foursquare' ),
			array( 'label' => 'Weibo', 'id' => 'weibo' ),
		);
		
		parent::__construct(
			'news_hub_social', // Base ID
			__('NewsHub: Social Links', 'news_hub'), // Name
			array( 'description' => __( 'Displays iconic links to your social media websites like facebook and twitter.', 'news_hub' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
       
		echo $args['before_widget'];

		// title
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		// description
		if ( ! empty( $instance['description'] ) ) {
			?>
			<p><?php echo $instance['description']; ?></p>
		<?php } ?>
		
		<?php if( !empty( $this->supported_social_media ) ){ ?>
			<div class="news_hub-social-links">
				<ul>
					<?php foreach( $this->supported_social_media as $index => $social_media ){ ?>
						<?php $$social_media['id'] = apply_filters( $social_media['id'], $instance[ $social_media['id'] ] ); ?>
						<?php if( !empty( $$social_media['id'] ) ){ ?>
							<li class="news_hub-social-links-item">
								<a class="<?php echo $social_media['id']; ?>" href="<?php echo esc_attr( $$social_media['id'] ); ?>">
									<i class="fa fa-<?php echo $social_media['id']; ?>"></i>
								</a>
							</li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>	

		<?php echo $args['after_widget']; 
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		
		// widgets administration form
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Connect With Us', 'news_hub' );
		$description = ! empty( $instance['description'] ) ? $instance['description'] : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'news_hub'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('description'); ?>">
				<?php _e('Description:', 'news_hub'); ?>
			</label>
			<textarea id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" class="widefat" rows="8" cols="20" placeholder="<?php _e('Enter a brief introduction about your external social media links.', 'newshub');?>"><?php echo esc_attr($description); ?></textarea>
		</p>
		<p>
			<em>
				<?php _e( 'Enter the url for each of the social media field. Leave blank to disable. Start with \'http://\'','news_hub' ); ?>
			</em>
		</p>
		
		<?php foreach( $this->supported_social_media as $key => $social_media ){ 
		
			 $label = esc_attr( $social_media['label'] ); 
			 $id = esc_attr( $social_media['id'] );
			 $$id = isset( $instance[ $id ] ) ? $instance[ $id ] : $title = ''; 
			 $placeholder = 'http://'; 
			 
		?>
		
			<p>
				<label for="<?php echo $this->get_field_id( $id ); ?>"><?php echo $label; ?>: </label> 
				<input placeholder="<?php echo $placeholder; ?>" class="widefat" id="<?php echo $this->get_field_id( $id ); ?>" name="<?php echo $this->get_field_name( $id ); ?>" type="text" value="<?php echo esc_attr(  $$id ); ?>">
			</p>
			
		<?php } //exfor
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	 
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		
		// update title
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		// update description
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		
		// iterate through each social media value
		// and assign the parameters on $instance 
		foreach( $this->supported_social_media as $key => $social_media ){ 
			$id = esc_attr( $social_media['id'] );
			$instance[$id] = ( ! empty( $new_instance[ $id ] ) ) ? strip_tags( $new_instance[$id] ) : '';
		} 
		
		return $instance;
		
	}

	/**
	 * Registers the widget into WordPress
	 *
	 * @uses  register_widget() registers the widget
	 * @return object self::
	 */
	public static function registerWidget(){
		
		register_widget(__CLASS__);

	}

} // class news_hub_social
?>