<?php
/**
 * Popular Videos Widgets Form
 *
 * @package NewsHub
 * @subpackage Widgets
 * @author dunhakdis
 */
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>">
		<?php _e( 'Title:' ); ?>
	</label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $this->vars->title ); ?>">
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'max_posts' ); ?>">
		<?php _e( 'How many videos to show?' ); ?>
	</label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'max_posts' ); ?>" name="<?php echo $this->get_field_name( 'max_posts' ); ?>" type="text" value="<?php echo esc_attr( $this->vars->max_posts ); ?>">
</p>