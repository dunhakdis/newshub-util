<?php
/**
 * Skills Shortcode Template
 *
 * @package NewsHub
 * @version 1.0
 */
?>
<?php

$color = $this->vars['color'];
$width = (int)$this->vars['percent'];
$label = esc_attr($this->vars['label']);
?>
<div class="news-hub-skills-details clearfix uppercase">
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<?php echo esc_attr($label); ?>
		</div>
		<div class="col-md-6 col-sm-6">
			<span class="pull-right">
				<?php echo $width; ?>%
			</span>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="progress">
	<div class="progress-bar progress-bar-success" style="background-color:<?php echo $color; ?>; width: <?php echo $width; ?>%;">
		<span class="sr-only"><?php echo $width;?>% <?php _e('Complete (success)', 'newshub');?></span>
	</div>
</div>
