<?php 
$classes = array('first', 'second');
$counter = 0;

global $wp_query;
?>
<style>
.news-hub-reviews[max-width="600px"] .first-column,
.news-hub-reviews[max-width="600px"] .first,
.news-hub-reviews[max-width="600px"] .second,
.news-hub-reviews[max-width="600px"] .col-md-6,
.news-hub-reviews[max-width="600px"] .second-column {
    width: 100%;
    margin-bottom: 15px;
}
.news-hub-reviews-wrap[max-width="600px"] .entry-post.col-md-6.first,
.news-hub-reviews-wrap[max-width="600px"] .entry-post.col-md-6.second {
    padding-left: 0;
    padding-right: 15px;
}
</style>
<div class="news-hub-reviews">
<?php $posts_per_page = $wp_query->query_vars['posts_per_page']; //-1 since $counter begins at 0 ?>

<?php if (have_posts()) { ?>
  <div class="news-hub-reviews-wrap">
    <?php while(have_posts()){ ?>
        <?php $counter++; ?>
        <?php //show bigger section for first two entries ?>
        <?php the_post(); ?>

        <?php if ($counter == 1){?>
           <div class="first-column col-md-7 col-sm-12">
        <?php } ?>
        <?php if ($counter < 3) { ?>
                    <div class="col-md-6 col-sm-6 entry-post <?php echo $classes[$counter-1]; ?>">
                        <a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
                            <?php if (has_post_thumbnail()) { ?>
                                <?php echo the_post_thumbnail('entry-post-thumbnail'); ?>
                            <?php } else { ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/entry-post-thumbnail.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>">
                            <?php } ?>
                        </a>

                        <h6>
                            <a class="black" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
                                <?php echo esc_attr(the_title()); ?>
                            </a>
                        </h6>


                        <div class="row details">
                            <div class="rating col-md-5 rm-padding-right">
                                <?php if (function_exists('news_hub_the_rating')){?>
                                    <?php news_hub_the_rating(); ?>
                                <?php } ?>
                            </div>

                            <div class="review-details w-featured-image-details col-md-7 rm-padding-left">
                                <ul class="pull-right">
                                    <?php if (function_exists('news_hub_get_the_date')){ ?>
                                    <li>
                                        <span class="fa fa-calendar-o"></span>
                                        <span class="entry-date"><?php echo news_hub_get_the_date(); ?></span>
                                    </li>
                                    <?php } ?>
                                    <li><span class="fa fa-comment-o"></span> <span><?php echo number_format_i18n( get_comments_number() ); ?></span></li>
                                </ul>
                            </div>
                        </div>
                        
                        <?php echo the_excerpt(); ?>
                    </div>
               
        <?php } else { ?>
            <?php if ($counter == 3) { ?>
                </div><!--.col-md-6-->
                <div class="col-md-5 col-sm-12">
            <?php } ?>
                <div class="row entry-post">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
                            <?php if (has_post_thumbnail()) { ?>
                                <?php echo the_post_thumbnail('thumbnail'); ?>
                            <?php } else { ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/entry-post-thumbnail.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>">
                            <?php } ?>
                        </a>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <h6>
                            <a class="black" href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
                                <?php echo the_title(); ?>
                            </a>
                        </h6>
                         <div class="clearfix">
                            <?php if (function_exists('news_hub_the_rating')) { ?>
                            <div class="rating pull-left">
                                <?php news_hub_the_rating(); ?>
                            </div>
                            <?php } ?>
                            <div class="w-featured-image-details pull-left">
                                <?php if (function_exists('news_hub_get_the_date')) { ?>
                                <ul class="pull-right">
                                    <li>
                                        <span class="fa fa-calendar-o"></span>
                                        <span class="entry-date"><?php echo news_hub_get_the_date(); ?></span>
                                    </li>
                                </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div><!--.entry-post-->
               
            <?php if ($counter == $posts_per_page ) { ?>
                </div>
            <?php } ?>

        <?php } //end else?>
    <?php } //endwhile ?>


            <?php if (1 === $counter) { ?>
                </div>
            <?php } ?>


    <div class="clearfix"></div>
</div><!--.news-hub-reviews-wrap-->
<?php } else { ?>
<div class="wpb_alert wpb_content_element vc_alert_square wpb_alert-info">
    <div class="messagebox_text">
        <p><?php _e('There are no reviews to show at this time.', 'news_hub'); ?></p>
    </div>
</div>
<?php } ?>
</div><!--.news-hub-reviews-->
<?php wp_reset_query(); ?>
<div class="clearfix "></div>