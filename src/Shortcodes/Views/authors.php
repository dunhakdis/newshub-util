<?php
/**
 * Authors Shortcode Template
 *
 * @version  1.0
 */
?>
<?php
// get the maximum author count set on shortcode param

if (isset($this->attributes['max_item'])) {
	$max_author_count = (int) $this->attributes['max_item'];
} else {
	$max_author_count = $this->maxItem;
}

$authors = get_users( sprintf('orderby=nicename&role=author&number=%d', $max_author_count) );

// index (iteration)
$count = 1;
?>

<?php if (!empty($authors)) { ?>
<div class="clearfix">
	<div class="row">

		<?php foreach($authors as $author) { ?>

			<?php $meta = get_user_meta($author->ID); ?>
			<?php $url = esc_attr(get_author_posts_url($author->ID)); ?>

			<?php $author_fb = esc_url(get_the_author_meta('facebook', $author->ID)); ?>
		    <?php $author_twitter = esc_url(get_the_author_meta('twitter', $author->ID)); ?>
		    <?php $author_gplus = esc_url(get_the_author_meta('googleplus', $author->ID)); ?>
		    <?php $author_linkedin = esc_url(get_the_author_meta('linkedin', $author->ID)); ?>

			<div class="col-md-4 author-item">
				<div class="news-hub-author-shortcode">
					<div class="news-hub-author-shortcode-social-media">
						<ul class="rm-mg no-list">
							<?php if (!empty($author_fb)) {?>
								<li><a class="body-color" href="<?php echo $author_fb; ?>"><i class="fa fa-facebook"></i></a></li>
							<?php } ?>
							<?php if (!empty($author_twitter)) {?>
								<li><a class="body-color" href="<?php echo $author_twitter; ?>"><i class="fa fa-twitter"></i></a></li>
							<?php } ?>
							<?php if (!empty($author_linkedin)) {?>
								<li><a class="body-color" href="<?php echo $author_gplus; ?>"><i class="fa fa-google-plus"></i></a></li>
							<?php } ?>
							<?php if (!empty($author_gplus)) {?>
								<li><a class="body-color" href="<?php echo $author_linkedin; ?>"><i class="fa fa-linkedin"></i></a></li>
							<?php } ?>
							
						</ul>
					</div><!--.news-hub-author-shortcode-social-media-->

					<div class="author-details">
						<div class="hide author-social-media">
							<ul><ul>
						</div>

						<div class="author-name center">
							<h5>
								<a class="black" title="<?php echo esc_attr($author->display_name); ?>" href="<?php echo $url; ?>">
									<?php echo esc_attr($author->display_name); ?>
								</a>
							</h5>
						</div>
						<div class="author-avatar">
							<div class="center">
								<a class="black" title="<?php echo esc_attr($author->display_name); ?>" href="<?php echo $url; ?>">
									<?php echo get_avatar($author->ID, 120); ?>
								</a>
							</div>
						</div>
						<div class="author-bio">
							<p class="center">
								<?php echo $meta['description'][0]; ?> 
							</p>
						</div>
					</div><!--.author-details-->
				</div><!--.news-hub-author-shortcode-->
			</div><!--.col-md-4 author-item-->
			
			<?php if ($count % 3 == 0 ) { ?>
				<div class="clearfix visible-md visible-lg"></div>
			<?php } ?>

			<?php $count++ ;?>
		<?php } //endforeach?>
	
	</div><!--.row-->
</div><!--.container-->
<?php } ?>