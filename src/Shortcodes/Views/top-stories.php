<?php $has_template = false; ?>

<section class="content-area">
<div class="news-hub-archive top-stories">
	<?php if ($this->vars['title']) { ?>
	    <header class="page-header">
	        <h1 class="page-title">
	        	<?php echo $this->vars['title']; ?>
	       	</h1>
	    </header>
    <?php } ?>
<?php if (have_posts()) { ?>
<div class="top-stories-archive-result">
	<?php  if( $template = locate_template( 'content-archive.php' ) ) { ?>
        	<?php $has_template = TRUE; ?>
        <?php } else { ?>
        	<div class="alert">
        		<?php _e('Template file (content-archive.php) must be overridden in your theme.', 'news_hub'); ?>
        	</div>
		<?php } ?>  
    <?php while(have_posts()) { ?>
       	 	<?php the_post(); ?>   
          	<?php if ($has_template){ ?>
          		<?php load_template( $template, false ); ?>
          	<?php } ?>
    <?php } ?>
</div><!--.top-stories-archive-result-->    
<?php } else {// endif ?>
<br />
<div class="alert alert-danger">
    <?php _e('There are no posts to show at this moment.', 'news_hub'); ?>
</div>
<?php } ?>
 </div>
</section>