<?php
/**
 * Category Tabs Shortcode Template
 *
 * @version  1.0
 */

// --- 

$tabs = $this->vars['tabs'];
$style = $this->vars['style'];
$the_item_class = 'col-md-3 col-sm-3';
$limit = 4; 

if ('vertical' === $style) { 
	$the_item_class = 'col-md-4 col-sm-4';
	$limit = 3;
} 
?>



<div class="category-tabs <?php echo $style;?>">
	<?php if (!empty($tabs)) { ?>
	<ul class="category-tab-tabs">
		<li>
			<a  data-limit="<?php echo $limit; ?>" data-category-slug="all" class="active" href="#" title="<?php _e('View All', 'news_hub'); ?>">
				<?php _e('View All', 'news_hub'); ?>
			</a>
		</li>
		<?php foreach ($tabs as $tab) { ?>
			<?php $category = get_term_by('slug', $tab, 'category'); ?>
			<?php if (!empty($category)) { ?>
				<li>
					<a data-limit="<?php echo $limit; ?>" data-category-slug="<?php echo $tab; ?>" href="#" title="#">
						<?php echo $category->name;?>
					</a>
				</li>
			<?php } else { ?>
				<li>
					<a href="#" title="<?php echo $tab;?>">
						<?php echo $tab;?>
					</a>
				</li>
			<?php } ?>
		<?php } ?>
	</ul>
	<?php } ?>
	
	<div class="clearfix"></div>

	<div class="category-tab-post-list-preloader">
		<div class="spinner">
			<span class="fa fa-spinner fa-spin"></span> <?php _e('Loading &hellip;', 'news_hub'); ?>
		</div>
	</div>

	<div class="category-tab-post-list">
		<div class="wrap">
			<?php if (have_posts())	{ ?>
				<ul class="row category-tab-post-list-ul">
					<?php while (have_posts()) { ?>
					<?php the_post(); ?>
					<li class="<?php echo $the_item_class;?>">
						<a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
							<?php if (has_post_thumbnail()) { ?>
								<?php echo the_post_thumbnail('entry-post-thumbnail'); ?>
							<?php } else { ?>
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/entry-post-thumbnail.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>" />
							<?php } ?>
						</a>
						<p class="description">
							<a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo the_title(); ?>">
								<?php echo the_title(); ?>
							</a>
						</p>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
	</div>
</div>