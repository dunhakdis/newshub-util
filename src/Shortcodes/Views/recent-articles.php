<?php
/**
 * Recent Articles shortcode template
 *
 * @version  1.0
 * @package NewsHub
 * @subpackage shortcodes
 */


$template = $this->vars;
$counter = 0;
$thumbnail_collection = array();

//make sure its unique
$recent_article_carousel_id = 'recent-article-id-'.time();
?>
<!--Element Resize Support-->
<style>

.news-hub-recent-articles[max-width="600px"] .col-md-6{
    width: 100%;
    margin-bottom: 30px;
}

.news-hub-recent-articles[max-width="600px"] p:last-child{
	padding-bottom: 45px;
}

.news-hub-recent-articles[max-width="600px"] .news-hub-recent-article-shortcode.carousel-indicators {
	width: 100%;
	right: auto;
	padding: 0 15px;
}
</style>
<div class="news-hub-recent-articles">
<?php if (have_posts()) { ?>
<div class="carousel slide fading recent-article-carousel" data-ride="carousel" id="<?php echo $recent_article_carousel_id; ?>">
	<div class="carousel-inner">
	<?php while (have_posts()) { ?>
		<?php $counter ++; ?>
		<?php the_post(); ?>
		<?php $class = $counter === 1 ? 'active': ''; ?>
			<div class="item <?php echo $class; ?>">
				<div <?php echo post_class(array('entry-post')); ?>>
				<div class="col-md-6 col-sm-12">
					<div class="recent-articles-featured-image">
						<a title="<?php echo esc_attr(the_title()); ?>" href="<?php echo esc_url(the_permalink()); ?>">
							<?php if (has_post_thumbnail()) {?>
								<?php the_post_thumbnail('shortcode-size'); ?>
							<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/images/shortcode-size.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>" />
							<?php } ?>
						</a>
						<div class="recent-articles-controls">
							<a class="left" href="#<?php echo $recent_article_carousel_id;?>" role="button" data-slide="prev">
								<div class="recent-articles-controls-left">
									<div class="recent-articles-controls-left-circle">
									    <span class="fa fa-angle-left" aria-hidden="true"></span>
									    <span class="sr-only"><?php _e('Previous', 'news_hub'); ?></span>
									</div>
								</div>
							</a>
							<a class="right" href="#<?php echo $recent_article_carousel_id;?>" role="button" data-slide="next">
								<div class="recent-articles-controls-right">
									<div class="recent-articles-controls-left-right">
									    <span class="fa fa-angle-right" aria-hidden="true"></span>
									    <span class="sr-only"><?php _e('Next', 'news_hub'); ?></span>
									</div>
								</div>
							</a>
						</div>
					</div>

					<?php if (has_post_thumbnail()) {?>
						<?php $thumbnail_collection[] = get_the_post_thumbnail(get_the_ID(), 'thumbnail');?>
					<?php } else { ?>
						<?php $thumbnail_collection[] = '<img src="'.get_template_directory_uri().'/images/thumbnail.jpg" alt="'.__('Featured Image', 'news_hub').'" />'; ?>
					<?php } ?>

				</div>
				<div class="col-md-6 col-sm-12">
					<p class="spacer visible-xs visible-sm"></p>
					<div>
						<?php if (function_exists('news_hub_the_category_list')) { ?>
						<div class="pull-left">
							<?php $category_colour = news_hub_the_category_list(); ?>
						</div>
						<?php } ?>
						<div class="pull-right <?php echo $category_colour; ?>">
							<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" title="<?php _e('View Author Profile', 'news_hub'); ?>">
								<?php  echo get_avatar(get_the_author_meta('ID')); ?>
							</a>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix">
							<h5>
								<a class="black" href="<?php echo esc_url(the_permalink());?>" title="<?php echo esc_attr(the_title()); ?>">
									<?php echo esc_attr(the_title()); ?>
								</a>
							</h5>
							<div class="clearfix"></div>
							<div class="w-featured-image-details <?php echo $category_colour; ?>">
								<ul>
									<?php if (function_exists('news_hub_get_the_date')) {?>
                                        <li>
                                            <span class="fa fa-calendar-o"></span>
                                            <span class="entry-date"><?php echo news_hub_get_the_date(); ?></span>
                                        </li>
                                   	<?php } ?>    
                                        <li>|</li>

                                        <?php if( function_exists('zilla_likes') ) {?>
											<li><?php zilla_likes(); ?></li>
											<li>|</li>
										<?php } ?>

                                        <li><span class="fa fa-comment-o"></span> <?php echo number_format_i18n( get_comments_number() ); ?></li>
                                    </ul>
							</div>
							<?php the_excerpt(); ?>
					</div>		
						
				</div>
			<div class="clearfix"></div>
			</div><!--.entry-post-->
		</div><!--.item-->
	<?php } // end while ?>

	</div><!--.carousel-inner-->
	<?php if (!empty($thumbnail_collection)) { ?>
		<?php $counter = 0; //reset the counter ?>
		<div class="sr-onlsy">
			<div data-snap-to-carousel="<?php echo $recent_article_carousel_id . '-wrap'; ?>">
				<ol class="news-hub-recent-article-shortcode carousel-indicators">
					<?php foreach($thumbnail_collection as $thumbnail) { ?>
						<?php $counter++; ?>
						<li class="<?php echo ($counter === 1) ? 'active':''; ?>" data-target="#<?php echo $recent_article_carousel_id; ?>" data-slide-to="<?php echo $counter - 1; ?>">
							
								<?php echo $thumbnail; ?>
						</li>
					<?php } ?>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	<?php } ?>
</div>		
<?php } ?>
</div>