<?php
/**
 * Loop Shortcode Template
 *
 * @package NewsHub
 * @version 1.0
 */
?>
<?php $thumbnail_size = 'single-post-thumbnail-entry-xs';?>
<?php $size = esc_attr($this->vars['size']); ?>
<?php $spacing = (int)$this->vars['spacing']; ?>
<?php $type = esc_attr($this->vars['type']); ?>

<?php $loop_shortcode_style = sprintf('style="margin-left:-%dpx; margin-right:-%dpx;"', $spacing, $spacing); ?>

<style>
	.news-hub-entries.loop-shortcode[max-width="600px"] .news-hub-entries-list{
		width: 100%;
		margin-left: auto;
	}
</style>
<?php if (have_posts()) { ?>

	<?php if ('small' === $size) { ?>
		<?php $thumbnail_size = 'entry-post-thumbnail-hv-4'; ?>
	<?php } ?>
	
	<ul <?php echo $loop_shortcode_style; ?> class="news-hub-entries news-hub-loop loop-shortcode">
	<?php while(have_posts()) { ?>
		<?php the_post(); ?>
		<?php //type:article layout ?>
		<?php if ($type === 'article') { ?>
			<li class="news-hub-entries-list col-md-4 col-sm-6" style="padding:0 <?php echo $spacing;?>px <?php echo $spacing*2;?>px <?php echo $spacing;?>px;">
				<?php //taken directly from newshub content-entry.php ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(array('news-hub-entry', 'masonry', 'entry-post', $thumbnail_size)); ?>>
						<div class="featured-image">
							<div class="visible">
								<?php if (has_post_thumbnail()){ ?>
									<?php echo the_post_thumbnail($thumbnail_size); ?>
								<?php } else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $thumbnail_size; ?>.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>" class="attachment-entry-post-thumbnail wp-post-image" />
								<?php } ?>
							</div>
							<div class="entry-list-avatar">
								<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php echo esc_attr(get_the_author_meta('display_name')); ?>">
									<?php echo get_avatar(get_the_author_meta('ID'), 48); ?>
								</a>
							</div>
							
							<div class="w-featured-image-entry-header">
								<?php news_hub_the_entry_details( $add_title_permalink = TRUE ); ?>
							</div>
						</div>
				</article><!-- #post-## -->
			</li>
		<?php } ?>
		<?php //type::video layout ?>
		<?php if ($type === 'video') { ?>
			<li class="col-md-4 col-sm-6 news-hub-entries-list"  style="padding:0 <?php echo $spacing;?>px <?php echo $spacing*2;?>px <?php echo $spacing;?>px;">
				<div class="news-hub-entries-list-wrap video">
					<article id="post-<?php the_ID(); ?>" 
						<?php post_class(array('news-hub-entry', 'entry-post', 'video', 'entry-post-thumbnail')); ?>>
							<div class="featured-image">
								<div class="visible">
									<?php if (has_post_thumbnail()){ ?>
										<?php echo the_post_thumbnail('entry-post-thumbnail'); ?>
									<?php } else { ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/entry-post-thumbnail.jpg" alt="<?php _e('Featured Image', 'news_hub'); ?>" class="attachment-entry-post-thumbnail wp-post-image" />
									<?php } ?>
								</div>
								<div class="entry-list-play-icon">
									<span class="fa-stack fa-lg white">
										<i class="fa fa-circle-thin fa-stack-2x"></i>
										<i class="fa fa-play fa-stack-1x"></i>
									</span>
								</div>
								<div class="w-featured-image-entry-header">
									<?php news_hub_the_entry_video_details(); ?>
								</div>
							</div>
					</article><!-- #post-## -->
				</div>
			</li>		
		<?php } ?>
	<?php } ?>
	</ul>
<?php } ?>