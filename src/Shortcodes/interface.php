<?php
/**
 * NewsHub Shortcodes Interface
 *
 * @package NewsHub
 * @subpackage shortcodes
 * @version 1.0
 */

namespace news_hub\shortcodes;

Interface NewsHubShortcode{

	/**
	 * includes the template for the specific shortcode
	 */
	public function register();

	/**
	 * The shorcode handle
	 * @param  array $atts the shortcode attributes
	 * @return mix
	 */
	public function handler($atts);

	/**
	 * The shortcode template
	 * @return mix
	 */
	public function template();
}
?>