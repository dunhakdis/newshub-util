<?php
/**
 * This file configures the shortcode we have
 * created earlier to work with Visual Composer
 * User Interface.
 *
 * @version  1.0
 */

// make sure visual composer is active
if (!function_exists('vc_map')) { return; } 

/**
 * Author
 */
vc_map( array(
   "name" => __("NewsHub: Authors"),
   "base" => "news_hub_authors",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Authors"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 5,
         "description" => __("How many number of authors to show. Will only show users that has a role of \'Author\'.")
      )
   )
));



/**
 * Latest Videos
 */
vc_map( array(
   "name" => __("NewsHub: Latest Videos"),
   "base" => "news_hub_latest_videos",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 5,
         "description" => __("How many number of videos would you like to show? This will be sorted by latest.")
      )
   )
));

/**
 * Skill bar
 */
vc_map( array(
   "name" => __("NewsHub: SkillBar"),
   "base" => "news_hub_skills",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Label"),
         "admin_label" => true,
         "param_name" => "label",
         "value" => __("My Skill"),
         "description" => __("The label of your skill (e.g. 'CSS', 'Java', 'Design').")
      ),
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Percent"),
         "admin_label" => true,
         "param_name" => "percent",
         "value" => __("75"),
         "description" => __("Enter a number ranging from 0 ~ 100.")
      ),
        array(
         "type" => "colorpicker",
         "holder" => "",
         "class" => "",
         "heading" => __("Color"),
         "admin_label" => true,
         "param_name" => "color",
         "value" => "#abc766",
         "description" => __("Use the color picker to pick a color for the skill bar.")
      )
   )
));

/**
 * Random Posts
 */
vc_map( array(
   "name" => __("NewsHub: Random Posts"),
   "base" => "news_hub_random_post",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 6   ,
         "description" => __("How many number of posts would you like to show? This will be sorted by latest.")
      )
   )
));

/**
 * Top Stories
 */
vc_map( array(
   "name" => __("NewsHub: Top Stories"),
   "base" => "news_hub_top_stories",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 5   ,
         "description" => __("How many number of posts would you like to show? This will be sorted by latest.")
      )
   )
));

/**
 * Recent Articles
 */
vc_map( array(
   "name" => __("NewsHub: Recent Articles"),
   "base" => "news_hub_recent_articles",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 5   ,
         "description" => __("How many number of posts would you like to show? This will be sorted by latest.")
      )
   )
));


/**
 * Reviews
 */
vc_map( array(
   "name" => __("NewsHub: Reviews"),
   "base" => "news_hub_reviews",
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "category" => __('Content'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "admin_label" => true,
         "param_name" => "max_item",
         "value" => 5   ,
         "description" => __("How many number of posts would you like to show? Posts that do not have reviews won't be shown. This will be sorted by latest.")
      )
   )
));

/*
 * Loop
 */
vc_map( array(
   "name" => __("NewsHub: Loop"),
   "base" => "news_hub_loop",
   "category" => __('Content'),
   "icon" => plugins_url('../../assets/images/vc-logo.png', __FILE__ ),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Max Posts"),
         "param_name" => "max_item",
         "admin_label" => true,
         "value" => 5,
         "description" => __("How many number of items would you like to show?")
      ),
      array(
         "type" => "dropdown",
         "holder" => "",
         "class" => "",
         "admin_label" => false,
         "heading" => __("Type"),
         "param_name" => "size",
         "admin_label" => true,
         "value" => array(
         		'small', 'large',
         	),
         "description" => __("Select a size of the post, can either be small or large. However, if you select a 'video' for the following select field (Type) the size that will be used is 'small'.")
      ),
       array(
         "type" => "dropdown",
         "holder" => "",
         "class" => "",
         "heading" => __("Type"),
         "param_name" => "type",
         "admin_label" => true,
         "value" => array(
         		'article', 'video',
         	),
         "description" => __("Select the type of post you wanted to show. Can either be an article or a video."),
      ),
       array(
         "type" => "dropdown",
         "holder" => "",
         "class" => "",
         "heading" => __("Order By"),
         "param_name" => "orderby",
         "admin_label" => true,
         "value" => array(
         		'date', 'title',
         	),
         "description" => __("The sorting of the post items will be selected base on what? Select 'date' or 'alphabetical'"),
      ),
       array(
         "type" => "dropdown",
         "holder" => "",
         "class" => "",
         "heading" => __("Order"),
         "param_name" => "order",
         "admin_label" => true,
         "value" => array(
         		'ASC', 'DESC',
         	),
         "description" => __("How the post items will be sorted. Select 'ASC' for ascending, 'DESC' for descending."),
      ),
        array(
         "type" => "dropdown",
         "holder" => "",
         "class" => "",
         "heading" => __("Enable Sticky Posts"),
         "param_name" => "ignore_sticky_posts",
         "admin_label" => true,
         "value" => array(
         		'Yes', 'No',
         	),
         "description" => __("Show sticky posts?"),
      ),
        array(
         "type" => "textfield",
         "holder" => "",
         "class" => "",
         "heading" => __("Spacing"),
         "admin_label" => true,
         "param_name" => "spacing",
         "value" => 0,
         "description" => __("The spacing for each post item. Recommended spacing is between 0 to 20."),
      ),
   )
));
?>