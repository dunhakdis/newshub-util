<?php
/**
 * Dropcap
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;

require_once $config->getSourcePath() . 'Shortcodes/interface.php';

/**
 * Displays top stories via shortcode
 *
 * @version  1.0
 */
class Dropcap implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Dropcap';

	/**
	 * the maximum number of entry
	 * @var integer
	 */
	public $maxItem = 5;

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_dropcap';

	/**
	 * The vars we can pass into our template
	 * @var array
	 */
	private $vars = array();

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		
		$this->config = new \NewsHubConfig();

		add_action('init', array($this, 'register'));
	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Configurable options: max-item, title
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @param  $content the string wrap in shortcode
	 * @return object self
	 */
	public function handler($attributes, $content = null)
	{

		$this->attributes = shortcode_atts(
			array(
 	      			'border_color' => '#87af24',
 	      			'background_color' => '#87af24',
 	      			'text_color' => '#fff',
 	      			'type' => 'square',
      			), 
      		$attributes, 
      		$this->shortcodeName
      	);

		$valid_types = array('square', 'circular');

		if (!in_array($this->attributes['type'], $valid_types)) {
			$this->attributes['type'] = 'square';
		}
		
		
		$dropcap_style  = sprintf("border-color:%s;", esc_attr($this->attributes['border_color']));
		$dropcap_style .= sprintf("background-color:%s;", esc_attr($this->attributes['background_color']));
		$dropcap_style .= sprintf("color:%s;", esc_attr($this->attributes['text_color']));

		$template = '<span class="news-hub-dropcap-'.esc_attr($this->attributes['type']).'" style="'.$dropcap_style.'">'.$content.'</span>';

		return $template;

	}

	/**
	 * The template
	 * @return self
	 */
	public function template()
	{
		return $this;
	}

}
?>