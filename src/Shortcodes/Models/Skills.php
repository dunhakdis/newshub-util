<?php
/**
 * Skills Model
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;

require_once $config->getSourcePath() . 'Shortcodes/interface.php';

/**
 * Displays top stories via shortcode
 *
 * @version  1.0
 */
class Skills implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Skills';

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_skills';

	/**
	 * The vars we can pass into our template
	 * @var array
	 */
	private $vars = array();

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		
		$this->config = new \NewsHubConfig();

		add_action('init', array($this, 'register'));
	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Configurable options: max-item, title
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @return object self
	 */
	public function handler($attributes)
	{

		$this->attributes = shortcode_atts(
			array(
 	      			'percent' => 80,
 	      			'label' => __('Skill', 'newshub'),
 	      			'color' => '#abc766',
      			), 
      		$attributes, 
      		$this->shortcodeName
      	);
		
		$this->attributes['percent'] = (int) $this->attributes['percent'];

		// limit the skills from 0 - 100
		if ($this->attributes['percent'] < 0 ) {
			$this->attributes['percent'] = 0;
		}

		if ($this->attributes['percent'] > 100) {
			$this->attributes['percent'] = 100;
		}

		$this->vars = $this->attributes;

		$template = $this->template();

	

		return $template;

	}

	/**
	 * Loads the template for this shortcode
	 * @return object self
	 */
	public function template()
	{

		ob_start();
		
		include $this->config->getSourcePath() . 'Shortcodes/Views/skills.php';

		return $output = ob_get_clean();
	}

}
?>