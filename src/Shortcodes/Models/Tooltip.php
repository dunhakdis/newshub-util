<?php
/**
 * Tooltup
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;

require_once $config->getSourcePath() . 'Shortcodes/interface.php';

/**
 * Displays top stories via shortcode
 *
 * @version  1.0
 */
class Tooltip implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Tooltip';

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_tooltip';

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		
		$this->config = new \NewsHubConfig();

		add_action('init', array($this, 'register'));
	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Configurable options: max-item, title
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @return object self
	 */
	public function handler($attributes, $content = null)
	{

		$this->attributes = shortcode_atts(
			array(
 	      			'text' => __('Tooltip Text Here', 'news_hub'),
      			), 
      		$attributes, 
      		$this->shortcodeName
      	);
		
		$text = $this->attributes['text'];

		$template  = '<span class="news-hub-tooltip" data-toggle="tooltip" title="'.$text.'">';
			$template .= esc_attr($content);
		$template .= '</span>';

		return $template;

	}

	/**
	 * Loads the template for this shortcode
	 * @return object self
	 */
	public function template()
	{
		// just return. no need to load 
		// full pledge template for this one
		return $this;
	}

}
?>