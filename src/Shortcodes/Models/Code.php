<?php
/**
 * Code
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;

require_once $config->getSourcePath() . 'Shortcodes/interface.php';

use NewsHubConfig;

/**
 * Formats the <pre> tags, highlights the code
 *
 * @version  1.0
 */
class Code implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Code';

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_code';

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		
		$this->config = new \NewsHubConfig();

		add_action('init', array($this, 'register'));
		add_action('wp_enqueue_scripts', array($this, 'code_prettyfier'));
		//remove_filter( 'the_content', 'wpautop' );
	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Handles the shortcode
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @param  $content the string wrap in shortcode
	 * @return object self
	 */
	public function handler($attributes, $content = null)
	{

		$this->attributes = shortcode_atts(
			array(
 	      			'language' => 'php',
      			),
      		$attributes, 
      		$this->shortcodeName
      	);

		$template = '<pre class="prettyprint">'.str_replace(array('<br>', '<br />','<p>','</p>'), '', $content).'</pre>';
		
		return $template;
	}


	/**
	 * Code Prettyfier
	 * @return void
	 */
	public function code_prettyfier() {

		$config = new NewsHubConfig();

		$prettyfyCSSPath = $config->getPluginUrl() . 'assets/google-code-prettify/prettify.css';
		$prettyfyJSPath  = $config->getPluginUrl() . 'assets/google-code-prettify/run_prettify.js';
		
		wp_enqueue_style( 'newshub-util-shortcode-prettify-css', $prettyfyCSSPath, false, '', 'all' );
		wp_enqueue_script( 'newshub-util-shortcode-prettify-js', $prettyfyJSPath, false, '', 'all' );
	}

	/**
	 * The template
	 * @return self
	 */
	public function template()
	{
		return $this;
	}

}
?>