<?php
/**
 * Loop
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;


require_once $config->getSourcePath() . 'Shortcodes/interface.php';
	
/**
 * Displays recent articles via shortcode and 
 *
 * @version  1.0
 * @author   <codehaiku.io@gmail.com>
 */
class Loop implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Loop';

	/**
	 * the maximum number of entry
	 * @var integer
	 */
	public $maxItem = 3;	

	/**
	 * The size of the item in the loop
	 * can either be 'small' or 'large' 
	 * 
	 * @var string
	 */
	public $size = 'large';

	/**
	 * the type of the item in the loop
	 * can eihter be article or video
	 * 
	 * @var string
	 */
	public $type = 'article';

	/**
	 * the spacing of the item to each other
	 * @var integer
	 */
	public $spacing = 0;

	/**
	 * the sorting order of the loop
	 * either 'DESC' or 'ASC'
	 * @var string
	 */
	public $order = 'DESC'; //latest

	/**
	 * the sorting direction
	 * either 'date' or 'title'
	 */
	public $orderby = 'date';

	/**
	 * show sticky posts or not
	 * @var boolean
	 */
	public $ignoreSticky = TRUE;

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_loop';

	/**
	 * The vars we can pass into our template
	 * @var array
	 */
	private $vars = array();

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		add_action('init', array($this, 'register'));

		$this->config = new \NewsHubConfig();

	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Configurable options: max-item, title
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @return object self
	 */
	public function handler($attributes)
	{
		
		$this->attributes = shortcode_atts(
			array(
 	      			'max_item' => $this->maxItem,
 	      			'size' => $this->size,
 	      			'type' => $this->type,
 	      			'spacing' => $this->spacing,
 	      			'order' => $this->order,
 	      			'orderby' => $this->orderby,
 	      			'ignore_sticky_posts' => $this->ignoreSticky,
      			), 
      		$attributes, 
      		$this->shortcodeName
      	);

		// limit the option of sizes to large and small only
		$allowed_sizes = array('small', 'large');
		if (!in_array($this->attributes['size'], $allowed_sizes)) {
			$this->attributes['size'] = 'large';
		}
		
		// limit the option of types to 'article' and 'video'
		$allowed_types = array('article', 'video');
		if (!in_array($this->attributes['type'], $allowed_types)) {
			$this->attributes['type'] = 'article';
		}

		// do not allow unlimited option
		if ($this->attributes['max_item'] == 0) {
			$this->attributes['max_item'] = $this->maxItem;
		}

		// sticky posts
		if ($this->attributes['ignore_sticky_posts'] == 'Yes') {
			$this->attributes['ignore_sticky_posts'] = 0;
		} else {
			$this->attributes['ignore_sticky_posts'] = 1;
		}

		$settings = array(
				// do not allow more than 5 items in slider
				// it is breaking the layout -_-
				'posts_per_page' => ($this->attributes['max_item'] <= $this->maxItem) ? $this->attributes['max_item'] : $this->maxItem,
				'ignore_sticky_posts' => $this->attributes['ignore_sticky_posts'],
				'order' => $this->attributes['order'],
				'orderby' => $this->attributes['orderby'],
				'tax_query' => array(
					array(
						'taxonomy' => 'post_format',
						'field' => 'slug',
						'terms' => array( 'post-format-video' ),
						'operator' => 'NOT IN',
					),
				),
			);

		if ($this->attributes['type'] === 'video') {
			$settings['tax_query'][0]['operator'] = 'IN';
		}

		query_posts($settings);

			$this->vars = (array)$this->attributes;

				$template = $this->template();

		wp_reset_query();

		return $template;

	}

	/**
	 * Loads the template for this shortcode
	 * @return object self
	 */
	public function template()
	{

		ob_start();
		
			include $this->config->getSourcePath() . 'Shortcodes/Views/loop.php';

		return $output = ob_get_clean();
	}

}
?>