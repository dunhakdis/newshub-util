<?php
/**
 * RecentArticles
 *
 * @package NewsHub
 * @subpackage shortcode
 */

namespace news_hub\shortcodes;


require_once $config->getSourcePath() . 'Shortcodes/interface.php';
	
/**
 * Displays recent articles via shortcode and 
 *
 * @version  1.0
 * @author   <codehaiku.io@gmail.com>
 */
class CategoryTab implements NewsHubShortcode{

	/**
	 * The title of the shortcode
	 * @var string
	 */
	public $shortcodeTitle = 'Category Tab';

	/**
	 * the maximum number of entry
	 * @var integer
	 */
	public $categorySlugs = array();	

	/**
	 * the unique name of our shortcode
	 * @var string
	 */
	private $shortcodeName = 'news_hub_category_tabs';

	/**
	 * The vars we can pass into our template
	 * @var array
	 */
	private $vars = array();

	/**
	 * The shortcode attribute
	 * @var mixed
	 */
	private $attributes;

	/**
	 * this method serve as a wrapper for wordpress' 
	 * add_action('init') function
	 * 
	 * @return object self
	 */
	public function __construct() 
	{ 
		add_action('init', array($this, 'register'));

		// register json handler callback api
		add_action('wp_ajax_categoryTabsHandleRequest', array($this, 'categoryTabsHandleRequest'));
			// for non logged-in users
				add_action('wp_ajax_nopriv_categoryTabsHandleRequest', array($this, 'categoryTabsHandleRequest'));

		$this->config = new \NewsHubConfig();

	}

	/**
	 * registers the shortcode via add_shortcode
	 * @return object useful for chaining methods
	 */
	public function register()
	{

		add_shortcode($this->shortcodeName, array($this, 'handler'));

		return $this;

	}

	/**
	 * Configurable options: categories
	 *
	 * @param  array $attributes wordpress shortcode callback parameter
	 * @return object self
	 */
	public function handler($attributes)
	{

		$category_tabs = array();

		$this->attributes = shortcode_atts(
			array(
 	      			'categories' => 'uncategorized',
 	      			'style' => 'horizontal',
      			), 
      		$attributes, 
      		$this->shortcodeName
      	);

		// pull all the categories
		$categories = explode(',', $this->attributes['categories']);

		if (is_array($categories)) {
			if (!empty($categories)) {
				foreach ($categories as $category) {
					// remove all the white spaces
					$category_tabs[] = esc_attr(preg_replace('/\s+/', '', $category));
				}
			}
		} else {
			$category_tabs = array();
		}

		$this->vars = array(
				'tabs' => $category_tabs,
				'style' => $this->attributes['style'],
			);

		$stmt_config = array(
				'posts_per_page' => 4,
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
			);

		// change the number of post for vertical layouts
		if ('vertical' === $this->attributes['style']) {
			$stmt_config['posts_per_page'] = 3;
		}

		query_posts($stmt_config);

		$template = $this->template();

		wp_reset_query();

		return $template;

	}

	/**
	 * Loads the template for this shortcode
	 * @return object self
	 */
	public function template()
	{

		ob_start();
		
			include $this->config->getSourcePath() . 'Shortcodes/Views/category-tab.php';

		return $output = ob_get_clean();
	}

	/**
	 * Handles the request of user when navigating
	 * or clicking one of the items inside the category tabs
	 * 
	 * @return Void
	 */
	public function categoryTabsHandleRequest()
	{
		header('Content-Type: application/json');

		// will store the summarized html
		$summary_html = '';

		// how many posts per page?
		$limit = 4; // default
		$item_class = 'col-md-3 col-sm-3';
		$response = array(
				'results' => array(),
				'summary_html' => '',
				'message' => '',
			);


		// filter empty category
		if (!isset($_GET['category-slug'])) {
			$response['message'] = __('The category you have requested does not exists. Please check the slug of the category you have entered.', 'news_hub');
			die($this->encodeResult($response));
		}

		$category_slug = sanitize_title($_GET['category-slug']);

		if (isset($_GET['limit'])) {
			$limit = intval($_GET['limit']);
		}
		
		
		// begin the query
		$config = array(
				'category_name' => $category_slug,
				'posts_per_page' => $limit,
				'post_status' => 'publish',
			);

		// enable 'View All' and accept 'all' request value
		if ('all' === $_GET['category-slug']) {
			unset($config['category_name']);
		}
		
		// change the columns base on the given amount of limit
		$item_class = ($limit == 4) ? 'col-md-3 col-sm-3': 'col-md-4 col-sm-4';

		query_posts($config);

		if (have_posts()) {
			while(have_posts()) {
				the_post();
				$item = array(
						'id' => get_the_ID(),
					);
				$response['results'][] = $item;
				$summary_html .= $this->getHandledRequest($item_class);
			}
			if (!empty($response['results'])) {
				$response['summary_html'] = $summary_html;
				$response['message'] = __('Success', 'news_hub');
			}
			
		} else {
			$response['message'] = __('Ops! Sorry, there are no posts published in that category. Please check back soon! Thanks', 'news_hub');
		}

		// as always
		wp_reset_query();

		die($this->encodeResult($response));
	}

	/**
	 * Handled request template
	 *
	 * @param  $list_class the class of the list item
	 * @return string the template
	 */
	public function getHandledRequest($list_class = 'col-md-3 col-sm-3')
	{	
		ob_start();
		?>
		
			<li class="<?php echo $list_class; ?>">
				<a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo esc_attr(the_title()); ?>">
					<?php if (has_post_thumbnail()){ ?>
						<?php echo the_post_thumbnail('entry-post-thumbnail'); ?>
					<?php } else { ?>
						<?php $fallback_thumbnail = get_template_directory_uri() . '/images/entry-post-thumbnail.jpg';?>
						<img src="<?php echo esc_url($fallback_thumbnail); ?>" alt="<?php _e('thumbnail', 'news_hub'); ?>" />
					<?php } ?>
				</a>
				<p class="description">
					<a href="<?php echo esc_url(the_permalink()); ?>" title="<?php echo the_title(); ?>">
						<?php echo the_title(); ?>
					</a>
				</p>
			</li>
		
		<?php
		return ob_get_clean();
	}

	/**
	 * Formats the encoded json response to jsonp
	 * 
	 * @param  array  $response the collection of data
	 * @return string the jsonp formatted data
	 */
	public function encodeResult($response = array())
	{
		return 'categoryTabsCallback('.json_encode($response).');';
	}

}
?>