<?php
/**
 * Custom walker class to handle the
 * mega menu functionality 
 *
 * @author Joseph G. <codehaiku.io@gmail.com>
 * @package WordPress
 * @subpackage NewsHub
 * @since 1.0
 */

namespace news_hub\mega_menu;

/**
 * @import WordPress Walker class
 */
use Walker;

class NewsHubMegaMenu extends Walker{
    
    /**
     * Tells the Walker where to inherit it's parent and id values
     * @var array
     */
    var $db_fields = array(
        'parent' => 'menu_item_parent', 
        'id'     => 'db_id' 
    );

    /**
     * Register the new mega menu post type
     */
    public function __construct()
    {
        add_action('init', array($this, 'megaMenuCallback'));
    }

    /**
     * Overwrites the start_lvl function
     * of WordPress' Walker Class
     *
     * @uses  Walker::start_lvl
     * @return Object Self
     */
    public function start_lvl(&$output, $depth = 0, $id = 'object_id')
    {

        $output .= '<ul class="sub-menu">';

        return $this;
    }

    /**
     * Overwrites the end_lvl function
     * of WordPress' Walker Class
     *
     * @uses  Walker::end_lvl
     * @return Object Self
     */
    public function end_lvl(&$output, $depth = 0, $id = 'object_id')
    {
        $output .= '</ul>';

        return $this;
    }

    /**
     * Overwrites the end_el function
     * of WordPress' Walker Classs
     * 
     * The statements that runs after the element is traversed
     *
     * @uses  Walker::end_el
     * @return  Object Self
     */
    function end_el(&$output, $item, $depth=0, $args=array()) 
    {
        $output .= "</li>\n";

        return $this;
    }

    /**
     * Overwrites the start_el function
     * inside WordPress' Walker class
     *
     * The statement that runs when iterating
     * through each of the menu items
     *
     * @uses  Walker::start_el
     * @return  Object Self
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        ob_start();
        ?>
        <?php 
        if ('mega_menu' == $item->object) {
            $item->classes[] = 'menu-item-has-children';
        }
        ?>
        <li class="<?php echo ltrim(implode(' ', (array)$item->classes)); ?>">

            <?php $url = esc_url(!empty($item->url) ? $item->url: $item->guid); ?>
            <?php $title = esc_attr(!empty($item->title) ? $item->title: $item->post_title); ?>

            <?php // disable link on mega menu ?>

            <?php if ('mega_menu' == $item->object) { ?> 
            <?php $url = '#'; ?>
            <?php } ?>

            <?php if ('mega_menu' == $item->object && $depth >= 1) { ?>
                <p class="text-warning menu-permalink" href="<?php echo $url; ?>" title="<?php $title; ?>">
                    <?php _e('Mega menu cannot be assigned to sub-menus. Please make it as a parent menu.', 'news_hub'); ?>
                </p>
            <?php } else { ?>
                <a class="menu-permalink" href="<?php echo $url; ?>" title="<?php $title; ?>">
                    <?php echo $title; ?>
                </a>
            <?php } ?>
            

            <?php if ('mega_menu' == $item->object) { ?>
                <?php  // do not support sub menu for mega menu ?>
                <?php if ($depth >= 1) { ?>
                 <ul class="sub-menu sub-menu-mega hide">
                    <li class="mega-menu-list"> 
                        <div class="mega-menu-content">
                            <?php _e('Mega menu cannot be assigned to sub-menus. Please make it as a parent menu.', 'news_hub'); ?>
                        </div>
                    </li>
                </ul>
                <?php } else { ?>
                <?php //query the page content here ?>
                    <ul class="sub-menu sub-menu-mega">
                        <li class="mega-menu-list"> 
                            <div class="mega-menu-content">
                                <div class="clearfix">
                                    <?php $mega_menu = get_post($item->object_id); ?>
                                    <?php if (!empty($mega_menu->post_content)){ ?>
                                        <?php echo do_shortcode(nl2br($mega_menu->post_content));?>
                                    <?php } else { ?>
                                        <?php if (current_user_can('edit_post', $mega_menu->ID)){ ?>
                                            <p class="text text-info">
                                                <?php $url = sprintf(admin_url().'post.php?post=%d&action=edit', $mega_menu->ID); ?>
                                                <?php _e('Howdy! Looks like you don\'t have any contents here. ', 'news_hub'); ?>
                                                <a href="<?php echo esc_url($url); ?>" title="<?php _e('Edit Post','news_hub'); ?>">
                                                    <?php _e('Click here to start adding content into your mega menu.','news_hub'); ?>
                                                </a>
                                            </p>
                                        <?php } ?>
                                    <?php } ?>
                                </div>  
                            </div>
                        </li>
                    </ul>
                <?php } ?>
            <?php } ?>
            <?php
         $output .= ob_get_clean();

         return $this;
    }

    /**
     * Tell WordPress to load new custom post type
     * called 'mega_menu'
     * 
     * @return object self
     */
    public function megaMenuCallback()
    {

        /**
         * the labels and description for our new post type
         * @var array
         */
        $labels = array(
            'name'               => __( 'Mega Menu', 'Mega Menu', 'news_hub' ),
            'singular_name'      => __( 'Mega Menu', 'Mega Menu', 'news_hub' ),
            'menu_name'          => __( 'Mega Menus', 'Mega Menus', 'news_hub' ),
            'name_admin_bar'     => __( 'Mega Menu', 'add new on admin bar', 'news_hub' ),
            'add_new'            => __( 'Add New', 'Mega Menu', 'news_hub' ),
            'add_new_item'       => __( 'Add New Mega Menu', 'news_hub' ),
            'new_item'           => __( 'New Mega Menu', 'news_hub' ),
            'edit_item'          => __( 'Edit Mega Menu', 'news_hub' ),
            'view_item'          => __( 'View Mega Menu', 'news_hub' ),
            'all_items'          => __( 'All Mega Menus', 'news_hub' ),
            'search_items'       => __( 'Search Mega Menus', 'news_hub' ),
            'parent_item_colon'  => __( 'Parent Mega Menus:', 'news_hub' ),
            'not_found'          => __( 'No Mega Menus found.', 'news_hub' ),
            'not_found_in_trash' => __( 'No Mega Menus found in Trash.', 'news_hub' )
        );

        /**
         * the mega menu custom post type settings
         * @var array
         */
        $args = array(
            'labels'             => $labels,
            'public'             => 'show_in_nav_menus',
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => false,
            'rewrite'            => array( 'slug' => 'mega-menu' ),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-feedback',
            'supports'           => array( 'title', 'editor', 'revisions')
        );

        // register the mega_menu post type
        register_post_type( 'mega_menu', $args );

        return $this;
    }

}

// Instantly instantiate the Object XD
new NewsHubMegaMenu();
?>